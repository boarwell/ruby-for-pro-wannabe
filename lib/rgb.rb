def to_hex(r, g, b)
  [r, g, b].inject('#') do |hex, n|
    hex + n.to_s(16).rjust(2, '0')
  end
end

# def to_hex(r,g,b)
#   hex = '#'
#   [r,g,b].each do |n|
#     hex += n.to_s(16).rjust(2,'0')
#   end
#   hex
# end

def to_ints(hex)
  r, g, b = hex[1...3], hex[3...5], hex[5...7]
  [r,g,b].map(&:hex)
end

# def to_ints(hex)
#   r = hex[1...3]
#   g = hex[3...5]
#   b = hex[5...7]
# 
#   [r, g, b].map do |s|
#     s.hex
#   end
# end

# def to_ints(hex)
#   r = hex[1...3]
#   g = hex[3...5]
#   b = hex[5...7]
#   ints = []
#   [r,g,b].each do |n|
#     ints << n.hex
#   end
#   ints
# end

# def to_ints(hex)
#   r = hex[1...3]
#   g = hex[3...5]
#   b = hex[5...7]
#   [r.hex, g.hex, b.hex]
# end
