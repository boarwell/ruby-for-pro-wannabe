class Gate
  STASTIONS = %i[umeda juso mikuni].freeze
  FARES = [150, 190].freeze

  def initialize(name)
    @name = name
  end

  def enter(ticket)
    ticket.stamp(@name)
  end

  def exit(ticket)
    fare = calc_fare(ticket)
    fare <= ticket.fare
  end

  def calc_fare(ticket)
    from = STASTIONS.index(ticket.stamped_at)
    to = STASTIONS.index(@name)

    distance = to - from
    FARES[distance - 1]
  end
end
