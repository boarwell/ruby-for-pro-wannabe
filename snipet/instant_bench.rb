# frozen_string_literal: true

# 簡易的に処理速度を測りたい場合
# たぶんちゃんとしたベンチマークはちゃんとしたやりかたがあるとは思いますが

t1 = Time.now
# some procedures
puts(Time.now - t1)
