#!/bin/ruby
# frozen_string_literal: true

ret =
  begin
    puts 'begin'
   # 'OK'
  rescue StandardError
    'error'
  ensure
    puts 'ensure'
    'ensure'
  end

puts ret #=> "OK"
